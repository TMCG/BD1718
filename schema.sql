DROP TABLE categoria CASCADE;
DROP TABLE categoria_simples CASCADE;
DROP TABLE super_categoria CASCADE;
DROP TABLE constituida CASCADE;
DROP TABLE fornecedor CASCADE;
DROP TABLE produto CASCADE;
DROP TABLE fornece_sec CASCADE;
DROP TABLE corredor CASCADE;
DROP TABLE prateleira CASCADE;
DROP TABLE planograma CASCADE;
DROP TABLE evento_reposicao CASCADE;
DROP TABLE reposicao CASCADE;

CREATE TABLE categoria(
  nome varchar(80),
  PRIMARY KEY (nome)
);

CREATE TABLE categoria_simples(
  nome varchar(80),
  FOREIGN KEY (nome) REFERENCES categoria(nome) ON DELETE CASCADE,
  PRIMARY KEY (nome)
);

CREATE TABLE super_categoria(
  nome varchar(80),
  FOREIGN KEY (nome) REFERENCES categoria(nome) ON DELETE CASCADE,
  PRIMARY KEY (nome)
);

CREATE TABLE constituida(
  super_categoria varchar(80),
  categoria varchar(80),
  FOREIGN KEY (super_categoria) REFERENCES super_categoria(nome)  ON DELETE CASCADE,
  FOREIGN KEY (categoria) REFERENCES categoria(nome) ON DELETE CASCADE,
  PRIMARY KEY (super_categoria,categoria)
);

CREATE TABLE fornecedor(
  nif numeric(9,0) ,
  nome varchar(80) ,
  PRIMARY KEY (nif)
);

CREATE TABLE produto(
  ean numeric(13,0) ,
  design varchar(240),
  categoria varchar(80),
  forn_primario numeric(9,0),
  data date NOT NULL,
  FOREIGN KEY (categoria) REFERENCES categoria(nome),
  FOREIGN KEY (forn_primario) REFERENCES fornecedor(nif),
  PRIMARY KEY (ean)
);

CREATE TABLE fornece_sec(
  nif numeric(9,0),
  ean numeric(13,0),
  FOREIGN KEY (nif) REFERENCES fornecedor(nif),
  FOREIGN KEY (ean) REFERENCES produto(ean) ON DELETE CASCADE,
  PRIMARY KEY (nif,ean)
);

CREATE TABLE corredor(
  nro int,
  largura int,
  CHECK(largura >0 AND nro >0),
  PRIMARY KEY (nro)
);

CREATE TABLE prateleira(
  nro int,
  lado char(1),
  altura char(1),
  CHECK (lado = 'l' OR lado = 'r'),
  CHECK (altura ='t' OR altura = 'm' OR altura = 'b'),
  FOREIGN KEY (nro) REFERENCES corredor(nro),
  PRIMARY KEY (nro,lado,altura)
);

CREATE TABLE planograma(
  ean numeric(13,0),
  nro int,
  lado char(1),
  altura char(1),
  face int,
  unidades int,
  loc int,
  FOREIGN KEY (ean) REFERENCES produto(ean) ON DELETE CASCADE,
  FOREIGN KEY (nro,lado,altura) REFERENCES prateleira(nro,lado,altura),
  PRIMARY KEY (ean,nro,lado,altura)
);

CREATE TABLE evento_reposicao(
  operador varchar(80),
  instante timestamp,
  PRIMARY KEY (operador,instante)
);

CREATE TABLE reposicao(
  ean numeric(13,0),
  nro int,
  lado char(1),
  altura char(1),
  operador varchar(80),
  instante timestamp,
  unidades int,
  FOREIGN KEY (ean,nro,lado,altura) REFERENCES planograma(ean,nro,lado,altura) ON DELETE CASCADE,
  FOREIGN KEY (operador,instante) REFERENCES evento_reposicao(operador,instante),
  PRIMARY KEY (ean,nro,lado,altura,operador,instante)
);

DROP FUNCTION super_colapse() CASCADE;
CREATE FUNCTION super_colapse() RETURNS
  TRIGGER AS
$BODY$
DECLARE super varchar;
BEGIN
  SELECT super_categoria INTO super
  FROM constituida
  WHERE super_categoria = OLD.super_categoria;
  IF super IS NULL THEN
    DELETE FROM super_categoria WHERE nome = OLD.super_categoria;
    IF OLD.super_categoria != super THEN
      INSERT INTO categoria_simples VALUES(OLD.super_categoria);
    END IF;
  END IF;
  RETURN OLD;
END;
$BODY$ LANGUAGE plpgsql;

CREATE TRIGGER super_colapse_check AFTER DELETE ON constituida
  FOR EACH ROW EXECUTE PROCEDURE super_colapse();


DROP FUNCTION categoria_delete() CASCADE;
CREATE FUNCTION categoria_delete() RETURNS
  TRIGGER AS
$BODY$
DECLARE prod NUMERIC(13,0);
DECLARE super VARCHAR(80);
DECLARE simple VARCHAR(80);
BEGIN
  SELECT nome into simple
  FROM categoria_simples
  WHERE nome = OLD.nome;
  IF simple IS NOT NULL THEN
    SELECT super_categoria INTO super
    FROM constituida
    WHERE OLD.nome = categoria;

    IF super IS NULL THEN
      FOR prod IN SELECT EAN FROM produto WHERE categoria = OLD.nome LOOP
        DELETE FROM produto WHERE ean = prod;
      END LOOP;
    END IF;
    IF super IS NOT NULL THEN
      FOR prod IN SELECT EAN FROM produto WHERE categoria = OLD.nome  LOOP
        UPDATE produto SET categoria = super WHERE ean = prod;
      END LOOP;
    END IF;
    RETURN OLD;
  END IF;
  SELECT nome into super
  FROM super_categoria
  WHERE nome = OLD.nome;
  IF super IS NOT NULL THEN
    SELECT super_categoria INTO simple
    FROM constituida
    WHERE OLD.nome = categoria;

    IF simple IS NULL THEN
      FOR prod IN SELECT EAN FROM produto WHERE categoria = OLD.nome LOOP
        DELETE FROM produto WHERE ean = prod;
      END LOOP;
    END IF;
    IF super IS NOT NULL THEN
      FOR prod IN SELECT EAN FROM produto WHERE categoria = OLD.nome  LOOP
        UPDATE produto SET categoria = simple WHERE ean = prod;
      END LOOP;
    END IF;
    RETURN OLD;
  END IF;
  FOR prod IN SELECT EAN FROM produto WHERE categoria = OLD.nome LOOP
    DELETE FROM produto WHERE ean = prod;
  END LOOP;
  RETURN OLD;
END;
$BODY$ LANGUAGE plpgsql;

CREATE TRIGGER  categoria_delete_check BEFORE DELETE ON categoria
  FOR EACH ROW EXECUTE PROCEDURE categoria_delete();

DROP FUNCTION listar_subs(super_cat VARCHAR(80));
CREATE OR REPLACE FUNCTION listar_subs(super_cat VARCHAR(80)) RETURNS TABLE(super_ct VARCHAR(80)) AS $BODY$

BEGIN
  RETURN QUERY (SELECT categoria
  FROM constituida
  WHERE super_categoria = super_cat);
END
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION forn_sec_not_prim() RETURNS
  TRIGGER AS
$BODY$
DECLARE prod numeric(13,0);
BEGIN
  SELECT ean INTO prod
  FROM produto
  WHERE ean = NEW.ean AND forn_primario = NEW.nif;
  IF prod IS NOT NULL THEN
    RAISE EXCEPTION 'Fornecedor secundario igual ao fornecedor primario do produto'
    USING HINT = 'Utilize um nif de fornecedor diferente do nif do fornecedor primario do produto';
  END IF;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

CREATE TRIGGER forn_sec_not_prim_check BEFORE INSERT ON fornece_sec
  FOR EACH ROW EXECUTE PROCEDURE forn_sec_not_prim();

CREATE OR REPLACE FUNCTION event_timestamp() RETURNS
  TRIGGER AS
$BODY$
DECLARE t timestamp;
BEGIN
  SELECT current_timestamp into t;
  IF t < NEW.instante THEN
    RAISE EXCEPTION 'Instante de evento reposicao no futuro'
    USING HINT = 'Utilize como instante uma data e hora anterior a atual';
  END IF;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

CREATE TRIGGER event_timestamp_check BEFORE INSERT ON evento_reposicao
  FOR EACH ROW EXECUTE PROCEDURE event_timestamp();
