-- BD Entrega 3 // Grupo 14 // 83458, 83535, 83567 // queries.sql

-- a) Qual o nome do fornecedor que forneceu o maior número de categorias?
-- Note que pode ser mais do que um fornecedor.

SELECT nome
FROM (
  SELECT MAX(c) as c
  FROM (
    SELECT nif, COUNT(*) AS c
    FROM (
      SELECT DISTINCT nif, categoria
      FROM fornece_sec
      NATURAL JOIN produto
      UNION
      SELECT DISTINCT nif, categoria
      FROM fornecedor
      NATURAL JOIN (
        SELECT forn_primario AS nif, categoria
        FROM produto) AS p
      ) AS forn_cat
    GROUP BY nif
  ) AS z
) AS max
NATURAL JOIN (
  SELECT nif, COUNT(*) AS c
  FROM (
    SELECT DISTINCT nif, categoria
    FROM fornece_sec
    NATURAL JOIN produto
    UNION
    SELECT DISTINCT nif, categoria
    FROM fornecedor
    NATURAL JOIN (
      SELECT forn_primario AS nif, categoria
      FROM produto) AS p
    ) AS forn_cat
  GROUP BY nif
) AS count
NATURAL JOIN fornecedor;

-- b) Quais os fornecedores primários (nome e nif) que forneceram produtos
-- de todas as categorias simples?
-- Para cada fornecedor primario verificamos se nao existe nenhuma categoria
-- que nao seja categoria simples.

SELECT DISTINCT f.nif, f.nome
FROM (
  SELECT DISTINCT forn.nif, forn.nome
  FROM (
    fornecedor
    NATURAL JOIN (
      SELECT DISTINCT forn_primario AS nif, categoria AS nome_cat
      FROM produto) AS fprim
  ) as forn
  WHERE NOT EXISTS(
    SELECT cat.nome AS nome_cat
    FROM categoria_simples AS cat
    WHERE NOT EXISTS(
      SELECT fprim2.nif
      FROM (
        fornecedor
        NATURAL JOIN (
          SELECT DISTINCT forn_primario AS nif, categoria AS nome_cat
          FROM produto) as fprim2
      )
      WHERE fprim2.nome_cat = cat.nome
      AND fprim2.nif = forn.nif
    )
  )
) AS f
NATURAL JOIN fornecedor;

-- c) Quais os produtos (ean) que nunca foram repostos?

SELECT ean
FROM produto
EXCEPT
SELECT ean
FROM reposicao;


-- d) Quais os produtos (ean) com um número de fornecedores secundários
-- superior a 10?

SELECT ean
FROM fornece_sec
GROUP BY ean
HAVING COUNT(ean) > 10;

-- e) Quais os produtos (ean) que foram repostos sempre pelo mesmo operador?

SELECT ean
FROM reposicao
GROUP BY ean
HAVING COUNT(operador) = 1;
