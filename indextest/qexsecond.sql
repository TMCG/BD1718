/*
SELECT DISTINCT F.nif,F.nome
FROM fornecedor F, produto P
WHERE F.nif = P.forn_primario
  AND P.categoria='Frutos';

SELECT ean,count(nif)
FROM produto P,fornece_sec F
WHERE P.ean = F.ean
GROUP BY P.ean
HAVING count(nif) > 1;
*/

EXPLAIN ANALYZE  SELECT P.ean,count(nif)
FROM produto P,fornece_sec F
WHERE P.ean = F.ean
GROUP BY P.ean
HAVING count(nif) > 1;

\d produto
\d fornecedor
\d fornece_sec

\qecho 'INDEX ean_fornece_sec_idx'
CREATE INDEX ean_fornece_sec_idx ON fornece_sec(nif);
EXPLAIN ANALYZE  SELECT P.ean,count(nif)
FROM produto P,fornece_sec F
WHERE P.ean = F.ean
GROUP BY P.ean
HAVING count(nif) > 1;

\d fornece_sec

DROP INDEX ean_fornece_sec_idx;
\o
