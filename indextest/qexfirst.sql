/*
SELECT DISTINCT F.nif,F.nome
FROM fornecedor F, produto P
WHERE F.nif = P.forn_primario
  AND P.categoria='Frutos';

SELECT ean,count(nif)
FROM produto P,fornece_sec F
WHERE P.ean = F.ean
GROUP BY P.ean
HAVING count(nif) > 1;
*/

EXPLAIN ANALYZE  SELECT DISTINCT F.nif,F.nome
FROM fornecedor F, produto P
WHERE F.nif = P.forn_primario
  AND P.categoria='Frutos';

\d produto
\d fornecedor
\d fornece_sec
\qecho 'INDEX categoria_idx'
CREATE INDEX categoria_idx ON produto(categoria);
EXPLAIN ANALYZE  SELECT DISTINCT F.nif,F.nome
FROM fornecedor F, produto P
WHERE F.nif = P.forn_primario
  AND P.categoria='Frutos';

\d produto
\qecho 'INDEX forn_primario_idx'
CREATE INDEX forn_primario_idx ON produto(forn_primario);
EXPLAIN ANALYZE  SELECT DISTINCT F.nif,F.nome
FROM fornecedor F, produto P
WHERE F.nif = P.forn_primario
  AND P.categoria='Frutos';

\d produto
DROP INDEX categoria_idx;
DROP INDEX forn_primario_idx;

\qecho 'INDEX forn_primario_idx'
CREATE INDEX forn_primario_idx ON produto(forn_primario);
EXPLAIN ANALYZE  SELECT DISTINCT F.nif,F.nome
FROM fornecedor F, produto P
WHERE F.nif = P.forn_primario
  AND P.categoria='Frutos';


\d produto
DROP INDEX forn_primario_idx;
\o
