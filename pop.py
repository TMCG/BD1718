import random
import string
forn_num = 200
prod_num = 10000
cor_num = 10
event_rep_num = 10000
categoria = [
"'Alimentos'",
    "'Bebidas'",
        "'S/Gas'","'Agua'","'Sumos'",
        "'C/Gas'","'Refrigerantes'",
        "'Alcoolicas'",
    "'Lacticinios'","'Iogurtes'",
    "'Frescos'","'Legumes'","'Frutos'","'Carne'","'Peixe'",
    "'Congelados'","'Pre-feitos'","'Sobremesas'",
    "'Doces'","'Gelados'","'Cereais'","'Bolachas'","'Chocolates'",
"'Vestuario'",
    "'T-Shirts'","'Calcas'","'Malas'","'Mochilas'","'Sapatos'"]
categoria_simples = ["'Agua'","'Sumos'","'Refrigerantes'","'Iogurtes'",
"'Legumes'","'Frutos'","'Carne'","'Peixe'","'Pre-feitos'","'Sobremesas'",
"'Gelados'","'Cereais'","'Bolachas'","'Chocolates'","'S/Gas'","'C/Gas'",
"'Alcoolicas'","'T-Shirts'","'Calcas'","'Malas'","'Mochilas'","'Sapatos'"]
super_categoria = ["'Alimentos'","'Bebidas'",
"'Lacticinios'","'Frescos'","'Congelados'","'Doces'","'Vestuario'"]
constituida = [
("'Alimentos'","'Bebidas'"),
    ("'Bebidas'","'S/Gas'"),("'Bebidas'","'C/Gas'"),("'Bebidas'","'Alcoolicas'"),
("'Alimentos'","'Lacticinios'"),
    ("'Lacticinios'","'Iogurtes'"),
("'Alimentos'","'Frescos'"),
    ("'Frescos'","'Legumes'"),("'Frescos'","'Frutos'"),("'Frescos'","'Carne'"),("'Frescos'","'Peixe'"),
("'Alimentos'","'Congelados'"),
    ("'Congelados'","'Pre-feitos'"),("'Congelados'","'Sobremesas'"),
("'Alimentos'","'Doces'"),
    ("'Doces'","'Gelados'"),("'Doces'","'Cereais'"),("'Doces'","'Bolachas'"),("'Doces'","'Chocolates'"),
("'Vestuario'","'T-Shirts'"),("'Vestuario'","'Calcas'"),("'Vestuario'","'Malas'"),
("'Vestuario'","'Mochilas'"),("'Vestuario'","'Sapatos'")]
fornecedor = []
for i in range(forn_num):
    nif = i + 1
    nome = "'"
    for j in range(random.randint(5,9)):
        rand = random.randint(0,1)
        if rand == 0:
            nome += string.ascii_lowercase[random.randint(0,25)]
        else:
            nome += string.ascii_uppercase[random.randint(0,25)]
    nome += "'"
    fornecedor += [(nif,nome)]

produto = []
catlen = len(categoria)
catsimp = len(categoria_simples)
supcat = len(super_categoria)
fornlen = len(fornecedor)
fornece_sec = []
for i in range(prod_num):
    ean = i + 100
    design = ""
    cat = ""
    if random.randint(1,100) >= 80:
        cat = super_categoria[random.randint(0,supcat-1)]
    else:
        cat = categoria_simples[random.randint(0,catsimp-1)]
    forn_primario = fornecedor[random.randint(0,fornlen - 1)]
    data = "'" +  "201" + str(random.randint(0,7))  + "/" + \
        str(random.randint(1,12)) +"/" +str(random.randint(1,28)) + "'"
    design = "'categoria: " + cat[1:-1] + " fornecedor: " + forn_primario[1][1:-1] + "'"
    produto += [(ean,design,cat,forn_primario[0],data)]
    for j in range(random.randint(1,6)):
        nif = fornecedor[rand][0]
        while (nif,ean) in fornece_sec or forn_primario[0] == nif:
            rand = random.randint(0,fornlen - 1)
            nif = fornecedor[rand][0]
        fornece_sec += [(nif,ean)]


prodlen = len(produto)
corredor = []
for i in range(cor_num):
    nro = i + 1
    largura = random.randint(5,20)
    corredor += [(nro,largura)]
prateleira = []
corlen = len(corredor)
alt = ["'t'","'m'","'b'"]
for i in range(corlen):
    nro = i + 1
    lado = ""
    altura = ""
    for j in range(3):
        #30% de nao criar uma prateleira
        if random.randint(1,100) <=70:
            altura = alt[j]
            prateleira += [(nro,"'l'",altura)]
    for j in range(3):
        if random.randint(0,100) <=70:
            altura = alt[j]
            prateleira += [(nro,"'r'",altura)]
planograma = []
pratlen = len(prateleira)
for i in range(prodlen):
    if random.randint(1,100) <=95:
        rand = random.randint(0,pratlen - 1)
        ean = produto[i][0]
        nro = prateleira[rand][0]
        lado = prateleira[rand][1]
        altura = prateleira[rand][2]
        face = random.randint(1,5)
        unidades = random.randint(5,30)
        loc = random.randint(1,corredor[prateleira[rand][0]-1][1])
        planograma += [(ean,nro,lado,altura,face,unidades,loc)]

evento_reposicao = []
operador = ["'Joao'","'Pedro'","'Ana'","'Tiago'","'Filipe'","'Marta'",
"'Sandra'","'Nuno'","'Vitor'","'Afonso'","'Maria'","'Joana'","'Ines'"]
oplen = len(operador)
for i in range(event_rep_num):
    op = operador[random.randint(0,oplen-1)]
    instante ="'2017-"+ str(random.randint(1,11)) + \
        "-" + str(random.randint(1,28)) + " " + \
        str(random.randint(1,23)) + ":" + str(random.randint(1,59)) + \
        ":" + str(random.randint(1,59)) + "'"
    if (op,instante) not in evento_reposicao:
        evento_reposicao += [(op,instante)]
reposicao = []
event_replen = len(evento_reposicao)
planlen = len(planograma)
for i in range(event_replen):
    rand = random.randint(0,planlen-1)
    if evento_reposicao[i][0] == "'Sandra'":
        ean = planograma[80][0]
        nro = planograma[80][1]
        lado = planograma[80][2]
        altura = planograma[80][3]
        operador = evento_reposicao[i][0]
        instante = evento_reposicao[i][1]
        unidades = planograma[80][5] - 4
    elif evento_reposicao[i][0] == "'Nuno'":
        ean = planograma[13][0]
        nro = planograma[13][1]
        lado = planograma[13][2]
        altura = planograma[13][3]
        operador = evento_reposicao[i][0]
        instante = evento_reposicao[i][1]
        unidades = planograma[13][5] - 4
    else:
        ean = planograma[rand][0]
        nro = planograma[rand][1]
        lado = planograma[rand][2]
        altura = planograma[rand][3]
        operador = evento_reposicao[i][0]
        instante = evento_reposicao[i][1]
        unidades = planograma[rand][5] - 4
    reposicao += [(ean,nro,lado,altura,operador,instante,unidades)]

print("START TRANSACTION;")

#categoria
print("\nINSERT INTO categoria")
print("VALUES")
for i in categoria:
    if(i == categoria[-1]):
        print("\t(",i,");")
    else:
        print("\t(",i,"),")

#categoria_simples
print("\nINSERT INTO categoria_simples")
print("VALUES")
for i in categoria_simples:
    if(i == categoria_simples[-1]):
        print("\t(",i,");")
    else:
        print("\t(",i,"),")

#super_categoria
print("\nINSERT INTO super_categoria")
print("VALUES")
for i in super_categoria:
    if(i == super_categoria[-1]):
        print("\t(",i,");")
    else:
        print("\t(",i,"),")

#constituida
print("\nINSERT INTO constituida")
print("VALUES")
for i in constituida:
    if(i == constituida[-1]):
        print("\t(",i[0],",",i[1],");")
    else:
        print("\t(",i[0],",",i[1],"),")

#fornecedor
print("\nINSERT INTO fornecedor")
print("VALUES")
for i in fornecedor:
    if(i == fornecedor[-1]):
        print("\t(",i[0],",",i[1],");")
    else:
        print("\t(",i[0],",",i[1],"),")

#produto
print("\nINSERT INTO produto")
print("VALUES")
for i in produto:
    if(i == produto[-1]):
        print("\t(",i[0],",",i[1],",",i[2],",",i[3],",",i[4],");")
    else:
        print("\t(",i[0],",",i[1],",",i[2],",",i[3],",",i[4],"),")

#fornece_sec
print("\nINSERT INTO fornece_sec")
print("VALUES")
for i in fornece_sec:
    if(i == fornece_sec[-1]):
        print("\t(",i[0],",",i[1],");")
    else:
        print("\t(",i[0],",",i[1],"),")

#corredor
print("\nINSERT INTO corredor")
print("VALUES")
for i in corredor:
    if(i == corredor[-1]):
        print("\t(",i[0],",",i[1],");")
    else:
        print("\t(",i[0],",",i[1],"),")

#prateleira
print("\nINSERT INTO prateleira")
print("VALUES")
for i in prateleira:
    if(i == prateleira[-1]):
        print("\t(",i[0],",",i[1],",",i[2],");")
    else:
        print("\t(",i[0],",",i[1],",",i[2],"),")

#planograma
print("\nINSERT INTO planograma")
print("VALUES")
for i in planograma:
    if(i == planograma[-1]):
        print("\t(",i[0],",",i[1],",",i[2],",",i[3],",",i[4],",",i[5],",",i[6],");")
    else:
        print("\t(",i[0],",",i[1],",",i[2],",",i[3],",",i[4],",",i[5],",",i[6],"),")

#evento_reposicao
print("\nINSERT INTO evento_reposicao")
print("VALUES")
for i in evento_reposicao:
    if(i == evento_reposicao[-1]):
        print("\t(",i[0],",",i[1],");\n")
    else:
        print("\t(",i[0],",",i[1],"),")

#reposicao
print("\nINSERT INTO reposicao")
print("VALUES")
for i in reposicao:
    if(i == reposicao[-1]):
        print("\t(",i[0],",",i[1],",",i[2],",",i[3],",",i[4],",",i[5],",",i[6],");")
    else:
        print("\t(",i[0],",",i[1],",",i[2],",",i[3],",",i[4],",",i[5],",",i[6],"),")

print("\nEND;")
