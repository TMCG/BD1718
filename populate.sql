START TRANSACTION;

INSERT INTO fornecedor VALUES(999999999, 'Alfredo CG lda.');
INSERT INTO produto
  VALUES (123456789010 ,'BEST Agua' , 'Agua' , 999999999 , '2016/7/20' ),
    (1234567890101 ,'BEST Sumos' , 'Sumos' , 999999999 , '2016/7/20' ),
    (1234567890102 ,'BEST Refrigerantes' , 'Refrigerantes' , 999999999 , '2016/7/20' ),
    (1234567890103 ,'BEST Iogurtes' , 'Iogurtes' , 999999999 , '2016/7/20' ),
    (1234567890104 ,'BEST Legumes' , 'Legumes' , 999999999 , '2016/7/20' ),
    (1234567890105 ,'BEST Fruta' , 'Fruta' , 999999999 , '2016/7/20' ),
    (1234567890106 ,'BEST Carne' , 'Carne' , 999999999 , '2016/7/20' ),
    (1234567890107 ,'BEST Peixe' , 'Peixe' , 999999999 , '2016/7/20' ),
    (1234567890108 ,'BEST Pre-feitos' , 'Pre-feitos' , 999999999 , '2016/7/20' ),
    (1234567890109 ,'BEST Sobremesas' , 'Sobremesas' , 999999999 , '2016/7/20' ),
    (1234567890110 ,'BEST Gelados' , 'Gelados' , 999999999 , '2016/7/20' ),
    (1234567890111 ,'BEST Cereais' , 'Cereais' , 999999999 , '2016/7/20' ),
    (1234567890112 ,'BEST Bolachas' , 'Bolachas' , 999999999 , '2016/7/20' ),
    (1234567890113 ,'BEST Chocolates' , 'Chocolates' , 999999999 , '2016/7/20' ),
    (1234567890114 ,'BEST T-Shirts' , 'T-Shirts' , 999999999 , '2016/7/20' ),
    (1234567890115 ,'BEST Calcas' , 'Calcas' , 999999999 , '2016/7/20' ),
    (1234567890116 ,'BEST Malas' , 'Malas' , 999999999 , '2016/7/20' ),
    (1234567890117 ,'BEST Mochilas' , 'Mochilas' , 999999999 , '2016/7/20' ),
    (1234567890118 ,'BEST Sapatos' , 'Sapatos' , 999999999 , '2016/7/20' );

INSERT INTO fornecedor VALUES(133333769, 'Filipe Wrecky Martins');
INSERT INTO produto
  VALUES (9999999999900 ,'BEST Agua' , 'Agua' , 133333769 , '2016/7/20' ),
    (9999999999901 ,'BEST Sumos' , 'Sumos' , 133333769 , '2016/7/20' ),
    (9999999999902 ,'BEST Refrigerantes' , 'Refrigerantes' , 133333769 , '2016/7/20' ),
    (9999999999903 ,'BEST Iogurtes' , 'Iogurtes' , 133333769 , '2016/7/20' ),
    (9999999999904 ,'BEST Legumes' , 'Legumes' , 133333769 , '2016/7/20' ),
    (9999999999905 ,'BEST Fruta' , 'Fruta' , 133333769 , '2016/7/20' ),
    (9999999999906 ,'BEST Carne' , 'Carne' , 133333769 , '2016/7/20' ),
    (9999999999907 ,'BEST Peixe' , 'Peixe' , 133333769 , '2016/7/20' ),
    (9999999999908 ,'BEST Pre-feitos' , 'Pre-feitos' , 133333769 , '2016/7/20' ),
    (9999999999909 ,'BEST Sobremesas' , 'Sobremesas' , 133333769 , '2016/7/20' ),
    (9999999999910 ,'BEST Gelados' , 'Gelados' , 133333769 , '2016/7/20' ),
    (9999999999911 ,'BEST Cereais' , 'Cereais' , 133333769 , '2016/7/20' ),
    (9999999999912 ,'BEST Bolachas' , 'Bolachas' , 133333769 , '2016/7/20' ),
    (9999999999913 ,'BEST Chocolates' , 'Chocolates' , 133333769 , '2016/7/20' ),
    (9999999999914 ,'BEST T-Shirts' , 'T-Shirts' , 133333769 , '2016/7/20' ),
    (9999999999915 ,'BEST Calcas' , 'Calcas' , 133333769 , '2016/7/20' ),
    (9999999999916 ,'BEST Malas' , 'Malas' , 133333769 , '2016/7/20' ),
    (9999999999917 ,'BEST Mochilas' , 'Mochilas' , 133333769 , '2016/7/20' ),
    (9999999999918 ,'BEST Sapatos' , 'Sapatos' , 133333769 , '2016/7/20' );

COMMIT;
