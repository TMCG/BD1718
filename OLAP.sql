--d_produto(prod_id,cean,categoria,nif_fornecedor_principal)
--d_tempo(time_id,dia,mes,ano)
--f_info(fact_id,prod_id,time_id)
DROP TABLE f_info;
DROP TABLE d_tempo;
DROP TABLE d_produto;
CREATE TABLE d_produto(
  cean numeric(13,0) , --Produto_id
  categoria varchar(80),
  nif_fornecedor_principal numeric(9,0),
  PRIMARY KEY(cean)
);
CREATE TABLE d_tempo(
  time_id numeric(8,0), --Tempo_id
  dia smallint,
  mes smallint,
  ano int,
  PRIMARY KEY(time_id)
);

CREATE TABLE f_info(
  cean numeric(13,0),
  time_id numeric(8,0),
  PRIMARY KEY(cean),
  FOREIGN KEY(cean) REFERENCES d_produto,
  FOREIGN KEY(time_id) REFERENCES d_tempo
);

/*
SELECT categoria,ano,mes,count(*)
FROM f_info
NATURAL JOIN d_tempo
NATURAL JOIN d_produto
WHERE nif_fornecedor_primario = 123455678
ROLLUP(ano,mes);
*/

/*
SELECT categoria,ano,mes,count(*)
FROM f_info
NATURAL JOIN d_tempo
NATURAL JOIN d_produto
WHERE nif_fornecedor_primario = 123455678
GROUP BY categoria,ano,mes
UNION
SELECT categoria,ano,NULL,count(*)
FROM f_info
NATURAL JOIN d_tempo
NATURAL JOIN d_produto
WHERE nif_fornecedor_primario = 123455678
GROUP BY categoria,ano
UNION
SELECT categoria,NULL,NULL,count(*)
FROM f_info
NATURAL JOIN d_tempo
NATURAL JOIN d_produto
WHERE nif_fornecedor_primario = 123455678
GROUP BY categoria
UNION
SELECT NULL,NULL,NULL,count(*)
FROM f_info
NATURAL JOIN d_tempo
NATURAL JOIN d_produto
WHERE nif_fornecedor_primario = 123455678;
*/
