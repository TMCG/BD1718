<html>
    <body>
<?php
    unset($super_name);
    unset($name);
    $name = $_REQUEST['cat'];
    $super_name = $_REQUEST['super_cat'];
    try
    {
        $host = "db.ist.utl.pt";
        $user ="ist424870";
        $password = "heyx0611";
        $dbname = $user;
        $db = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $db->query("start transaction;");
        /* Verificar se o nome da categoria a adicionar nao sao white spaces */
        if(!(trim($name) == '')){
          $sql = "INSERT INTO categoria VALUES(:name);";
          $stmt = $db->prepare($sql);
          $stmt->bindParam(':name', $name);
          $stmt->execute();

          $sql = "INSERT INTO categoria_simples VALUES(:name)";
          $stmt = $db->prepare($sql);
          $stmt->bindParam(':name', $name);
          $stmt->execute();

          if($super_name !== ''){
            $sql = "INSERT INTO constituida VALUES(:super_name, :name);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':super_name', $super_name);
            $stmt->execute();
          }

          echo("<p>Categoria ".$name." inserida com sucesso.</p>");
        } else {
          echo("<p>ERRO: Tem de preencher o campo 'Nome' da categoria a inserir.</p>");
        }
        
        //echo("<p>$sql</p>");
        echo("<p><a href='./app.php'>Voltar</a></p>");
        //$db->query($sql);
        $db->query("commit;");

        $db = null;
    }
    catch (PDOException $e)
    {
        $db->query("rollback;");
        echo("<p>ERROR: {$e->getMessage()}</p>");
    }
?>
    </body>
</html>
